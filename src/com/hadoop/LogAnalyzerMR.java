package com.hadoop;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class LogAnalyzerMR {

	//MAPPER
	//key=0 value=64.242.88.10 - - [07/Mar/2004:16:45:56 -0800] "GET /twiki/bin/attach/Main/PostfixCommands HTTP/1.1" 401 12846 
	//key = 64.242.88.10 value = 1
	public static class WcMapper extends Mapper<LongWritable, Text, Text, IntWritable>{
		Text outkey = new Text();
		IntWritable outvalue = new IntWritable();
		public void map(LongWritable key, Text value,Context context) throws IOException, InterruptedException{
			ApacheAccessLog log = ApacheAccessLog.parseFromLogLine(value.toString());
			outkey.set(log.getIpAddress());
			outvalue.set(log.getResponseCode());
			context.write(outkey, outvalue);
			System.out.println("map key="+outkey+" & map value="+outvalue);
		}
	}
	
	//REDUCER
	//64.242.88.10,(1,1,1,1,1,1,1)
	//64.242.88.10,8
	public static class WcReducer extends Reducer<Text, IntWritable, Text, IntWritable>{
		IntWritable outvalue = new IntWritable();
		public void reduce(Text key,Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
			int sum =0;
			for(IntWritable value : values){
				if(value.get() == 404)
					sum++;
			}
			if(sum > 0){
				outvalue.set(sum);
				context.write(key, outvalue);
				System.out.println("Reduce key="+key+" & Reduce value="+outvalue);
			}
		}
	}
	
	
	//DRIVER
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		Configuration conf = new Configuration();
		Job job = new Job(conf,"Log MapReduce program");
		
		job.setJarByClass(LogAnalyzerMR.class);
		job.setMapperClass(WcMapper.class);
		job.setReducerClass(WcReducer.class);
		
//		job.setCombinerClass(WcReducer.class);
		
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		System.exit(job.waitForCompletion(true)?0:1);
		
	}

}
